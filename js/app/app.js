(function() {
    'use strict';
	
    angular.module('app', []);

    angular.module('app')
        .constant('config', {
            desktopUrl: '//52.31.247.44:9999/coming_soon_desktop/',
            mobileUrl: '//52.31.247.44:9999/coming_soon_mobile/'
        });

    angular.module('app')
        .controller('MainController', MainController);

    MainController.$inject = ['$window', '$scope', 'config'];
    function MainController($window, $scope, config) {
        var vm = this;
		var fullpage = false;
        vm.mobile = false;

        var mq670 = $window.matchMedia( "(max-width: 670px)" );
        mq670.addListener(
            function(args) {
                $scope.$apply(function() {WidthChange(args)})
            }
        );
        WidthChange(mq670);

        // media query change        		
		function WidthChange(mq) {
			vm.mobile = !!mq.matches;
			if (vm.mobile) {
				vm.safari = !!$window.navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
			}
		}

        vm.clickCTA = function() {
            if (vm.mobile) {
                $window.location.href = config.mobileUrl;
            } else {
                $window.location.href = config.desktopUrl;
            }
        }
    }
})();